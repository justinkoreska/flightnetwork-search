# Flight Network Search CLI

A simple command-line interface for performing functions against FNXML web API.

Currently, only supports round trip search and avail.

## Usage

Don't forget to `npm install`!

#### Search
```
> node src/search.js yyz yvr 2020-01-01 2020-02-01 flightnetwork
searching yyz-yvr 2020-01-01 2020-02-01
session f81f9e517f4cd60da4bc4b0b51c30762
results status=InProgress count=0
results status=InProgress count=0
results status=InProgress count=0
results status=InProgress count=0
results status=InProgress count=0
results status=InProgress count=0
results status=InProgress count=550
results status=InProgress count=550
results status=Completed count=219
461	450.28	c8-P	AC/AC
427	457.8	c8-P	AC/WS
518	457.8	c8-P	WS/AC
506	487.63	c8-P	WS/WS
172	568.46	c1A	AC/AC/AC
279	681.49	c1A	AC/AC/AC/AC
?qid=f81f9e517f4cd60da4bc4b0b51c30762&cid=461
```
#### Avail
```

> node src/avail.js f81f9e517f4cd60da4bc4b0b51c30762 461
reviewing f81f9e517f4cd60da4bc4b0b51c30762/461
{ Code: 'ERR_SOLD_OUT',
  Type: '1.EWT',
  Message: 'We are sorry but the flight you have selected is Sold Out.' }
```
### TO DO

 * Use a lib for parsing named args (maybe https://www.npmjs.com/package/minimist)
 * Add support for one-way and multi-leg searches
 * Add options for results output (unique/filter/sort)
