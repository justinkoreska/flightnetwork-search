var fnxml = require("./fnxml");

const [
  commandName,
  scriptName,
  sid,
  cid,
] = process.argv;

console.log(`reviewing ${sid}/${cid}`);

const request = {
  sid,
  cid,
  //lang: "EN",
  //currency: "CAD",
  //ancillary_options: 0,
};

fnxml.handleResponse(fnxml.avail(request), response => {

  console.log(JSON.stringify(response));
});
