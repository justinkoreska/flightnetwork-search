const fetch = require("node-fetch");

const apiBase = "http://fnxml.flightnetwork.com/api";
const apiSearch = "/search/json";
const apiSearchAsync = "/search/async/json";
const apiSearchResults = "/search/results/json";
const apiReview = "/review/json";
const apiFlexdata = "/flexdata";

const toQueryString = obj => {
  var parts = [];
  for (var i in obj)
    if (obj.hasOwnProperty(i))
      parts.push(encodeURIComponent(i) + "=" + encodeURIComponent(obj[i]));
  return parts.join("&");
};

const makeFormPost = form => ({
  method: "POST",
  headers: {
    "Content-Type": "application/x-www-form-urlencoded"
  },
  body: toQueryString(form),
});

const handleResponse = (promise, callback) => {
  promise
    .then(response => {
      response.json()
        .then(json => {
          const errors =
            json.OTA_AirLowFareSearchRS && json.OTA_AirLowFareSearchRS.Errors ||
            json.OTA_AirPriceRS && json.OTA_AirPriceRS.Errors;
          if (errors)
            return Promise.reject(errors);

          if (callback) callback(json);
        })
        .catch(console.log);
    })
    .catch(console.log);
};

const search = request =>
  fetch(apiBase + apiSearch, makeFormPost(request));

const searchAsync = request =>
  fetch(apiBase + apiSearchAsync + "?" + toQueryString(request));

const searchResults = sid =>
  fetch(apiBase + apiSearchResults, makeFormPost({ sid }));

const avail = request =>
  fetch(apiBase + apiReview, makeFormPost(request));

const flexdata = sid =>
  fetch(apiBase + apiFlexdata + "/" + sid);

module.exports = {
  search,
  searchAsync,
  searchResults,
  avail,
  flexdata,
  handleResponse,
};
