var fnxml = require("./fnxml");

const [
  commandName,
  scriptName,
  fromAirport,
  toAirport,
  fromDate,
  toDate,
  clientRef,
] = process.argv;

const request = {
  client_ref: clientRef || "flightnetwork",
  trip_type: "Roundtrip",
  dep_from: fromAirport,
  dep_to: toAirport,
  departure_date: fromDate,
  return_date: toDate,
  trip_class: "Economy",
  currency: "USD",
  Adult: 1,
  Child: 0,
  Infant: 0,
  nonstop: 1,
  show_fare_family: 1,
  max_results: 0,
  show_source_price: 1,
  flex: 1,
};

const getResults = (sid, count, delay) => {
  setTimeout(_ => {
    fnxml.handleResponse(fnxml.searchResults(sid), response => {

      const results = response.OTA_AirLowFareSearchRS;
      const itineraries = results && results.PricedItineraries || [];
      const complete = "Completed" == results.Status || count <= 0;

      console.log(`results status=${results.Status} count=${itineraries.length}`);

      if (!complete) {
        getResults(sid, count - 1, delay);
      } else {
        if (itineraries.length) {
          for (let fare of findDistinct(10, "vendor", itineraries))
            console.log(`${fare.cid}\t${fare.price}\t${fare.vendor}\t${fare.airlines}`);

          console.log(`?qid=${sid}&cid=${itineraries[0].AirItineraryPricingInfo.QuoteID}`);
        } else {
          console.log("No flights found");
        }
      }
    })
  }, delay);
};

const findDistinct = (count, key, itineraries) => {
  const fares = itineraries.map(itinerary => ({
    cid: itinerary.AirItineraryPricingInfo.QuoteID,
    vendor: itinerary.AirItineraryPricingInfo.FareInfos.FareInfo.TPA_Extensions.ProviderCode,
    airlines: mapAirlines(itinerary.AirItinerary.OriginDestinationOptions.OriginDestinationOption),
    price: itinerary.AirItineraryPricingInfo.ItinTotalFare.TotalFare.Amount,
  }));
  const distinct = fares.reduce((acc, fare) => {
    if (!acc[fare[key]] && Object.keys(acc).length < count)
      acc[fare[key]] = fare;
    return acc;
  }, {});
  return Object.keys(distinct).map(key => distinct[key]);
};

const mapAirlines = legs =>
  legs.map(leg =>
    leg.FlightSegment.map(segment =>
      segment.MarketingAirline.Code
    ).join("/")
  ).join("/");


console.log(`searching ${fromAirport}-${toAirport} ${fromDate}-${toDate}`);

fnxml.handleResponse(fnxml.searchAsync(request), response => {
  let results = response.OTA_AirLowFareSearchRS;
  let sid = results.EchoToken;

  console.log(`session ${sid}`);

  getResults(sid, 10, 1000);
});
