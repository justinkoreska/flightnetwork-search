var fnxml = require("./fnxml");

const [
  commandName,
  scriptName,
  sid,
] = process.argv;

console.log(`getting flex data for ${sid}`);

fnxml.handleResponse(fnxml.flexdata(sid), response => {
    console.log("flex data", response);
});
